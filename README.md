<div align="center" id="top">

<img width="100px" src="zoetrope-logo.png" />

</div>

&#xa0;

<div align="center">

<p>
  <a href="#dart-about"> About </a> &#xa0; | &#xa0; 
  <a href="#computer-technologies-used"> Technologies used </a> &#xa0; | &#xa0; 
  <a href="#white_check_mark-requirements"> Requirements </a> &#xa0; | &#xa0; 
  <a href="#checkered_flag-starting"> Starting </a> &#xa0; | &#xa0; 
  <a href="#bulb-roadmap"> Roadmap </a> &#xa0; | &#xa0;
  <a href="#unlock-license"> License </a> &#xa0; | &#xa0;
  <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Developer </a>
</p>

</div>

<br>

## :dart: About ##

Zoetrope is a movie and series social networking app that connects users with similar tastes. This project was proposed in the <a href='https://ejcm.com.br/'> EJCM </a> selection process.

:art: See the prototyping of this project on my <a href="https://www.behance.net/gallery/130332499/Zoetrope"> Behance</a>.

<div align="center">
<img src="mockup.jpg" width="600px">
</div>

## :computer: Technologies used ##

- [HTML](https://www.w3schools.com/html/)
- [CSS](https://developer.mozilla.org/pt-BR/docs/Web/CSS)

## :white_check_mark: Requirements ##

Before starting :checkered_flag:, you need to have [Git](https://git-scm.com) installed.

## :checkered_flag: Starting ##

```bash
# Clone the folder.
$ git clone https://gitlab.com/luizalissandrarosa/zoetrope-landingpage

# Access it.
$ cd zoetrope-landingpage

```

## :bulb: Roadmap ##

- [x] Responsiveness.

## :unlock: License ##

This project is under license from MIT. To learn more, see [LICENSE](LICENSE). 

&#xa0;

<hr/>

Developed with ❤ by <a href="https://www.linkedin.com/in/luiza-lissandra/" target="_blank"> Luiza Lissandra :rocket: </a>

<a href="#top"> Back to top </a>
